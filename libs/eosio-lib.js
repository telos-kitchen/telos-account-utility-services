const { JsonRpc } = require("eosjs");
const fetch = require("node-fetch"); // node only; not needed in browsers

export async function genRandomKey() {
  const ecc = require("eosjs-ecc");
  let key = {};
  key.privateKey = await ecc.randomKey();
  key.publicKey = await ecc.privateToPublic(key.privateKey);
  return key;
}

export async function genRandomKeys(numKeys = 2) {
  let keys = [];
  for (var i = 0; i < numKeys; i++) {
    keys.push(await genRandomKey());
  }
  return keys;
}

export async function accountExists(accountName) {
  const rpc = new JsonRpc(process.env.eosioApiEndPoint, { fetch });
  try {
    await rpc.get_account(accountName);
    return true;
  } catch (e) {
    return false;
  }
}

export async function validAccountFormat(accountName) {
  // eslint-disable-next-line prettier/prettier
  // eslint-disable-next-line no-useless-escape
  var telosAccountRegex = RegExp("^([a-z]|[1-5]|[\.]){1,12}$", "g"); // does it match EOSIO account format?
  if (!telosAccountRegex.test(accountName)) {
    return false;
  }
  return true;
}
